///////////////////////////////////////////////////////////////////////////////////////////
// Numbers of keywords
// 01. Carousel
///////////////////////////////////////////////////////////////////////////////////////////
$(document).ready(function () {
  ///////////////////////////////////////////////////////////////////////////////////////////
  // 01. Carousel
  function carousel() {
    $("#photo_info .carousel .owl-carousel").owlCarousel({
      animateIn: "fadeIn",
      animateOut: "fadeOut",
      loop: true,
      nav: false,
      margin: 0,
      autoplayTimeout: 4000,
      autoplay: true,
      smartSpeed: 2000,
      dotsEach: false,
      pagination: false,
      dots: false,
      navText: false,
      responsive: {
        0: {
          items: 1,
        },
        600: {
          items: 1,
        },
        1000: {
          items: 1,
        },
      },
    });
    var owl = $("#photo_info .carousel .owl-carousel");

    $("#photo_info .carousel .customPrevBtn").click(function () {
      owl.trigger("prev.owl.carousel");
    });
    $("#photo_info .carousel .customNextBtn").click(function () {
      owl.trigger("next.owl.carousel");
    });

    $(document).keydown(function (event) {
      if (event.keyCode === 39) {
        owl.trigger("prev.owl.carousel");
      }
      if (event.keyCode === 37) {
        owl.trigger("next.owl.carousel");
      }
    });
  }
  $("#photo_info").length && carousel();

  function carousel_blog() {
    $("#carousel_blog .carousel .owl-carousel").owlCarousel({
      //   animateIn: "fadeIn",
      //   animateOut: "fadeOut",
      loop: true,
      nav: false,
      margin: 19,
      autoplayTimeout: 4000,
      autoplay: true,
      smartSpeed: 2000,
      dotsEach: false,
      pagination: false,
      dots: false,
      navText: false,
      responsive: {
        0: {
          items: 1,
        },
        600: {
          items: 2,
        },
        1000: {
          items: 3,
        },
      },
    });
    var owl = $("#photo_info .carousel .owl-carousel");

    $("#carousel_blog .carousel .customPrevBtn").click(function () {
      owl.trigger("prev.owl.carousel");
    });
    $("#carousel_blog .carousel .customNextBtn").click(function () {
      owl.trigger("next.owl.carousel");
    });

    $(document).keydown(function (event) {
      if (event.keyCode === 39) {
        owl.trigger("prev.owl.carousel");
      }
      if (event.keyCode === 37) {
        owl.trigger("next.owl.carousel");
      }
    });
  }
  $("#carousel_blog").length && carousel_blog();
});
