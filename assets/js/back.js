const BASE_URL = 'http://162.55.209.61:5000/api';

async function sendRequest(url, body){
    return fetch(url, {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(body)
    });
}

function responseMessage(res, response){
    toastr.clear();
    if(res.ok){
        toastr.success(response.message);
    } else {
        toastr.error(response.message);
    }
}

async function contactUs(event){
    event.preventDefault();
    const name = document.getElementById('name_contact');
    const email = document.getElementById('email_contact');
    const message = document.getElementById('message_contact');

    const body = {
        name: name.value,
        email: email.value,
        message: message.value
    }

    document.getElementById('contact_button').disabled = true;

    const res = await sendRequest(`${BASE_URL}/contact`, body);
    const response = await res.json();
    responseMessage(res, response);
    document.getElementById('contact_button').disabled = false;

    name.value = '';
    email.value = '';
    message.value = '';
}

async function subscribe(event){
    event.preventDefault();

    const email = document.getElementById('subs');

    const body = {
        email: email.value
    }

    document.getElementById('subscribe_button').disabled = true;
    const res = await sendRequest(`${BASE_URL}/subscribe`, body);
    const response = await res.json();
    responseMessage(res, response);
    document.getElementById('subscribe_button').disabled = false;

    email.value = '';

}
